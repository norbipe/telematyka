package com.norbipe;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class PatientBaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_patientbase);
    }
}