package com.norbipe;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        TextView textView = (TextView) findViewById(R.id.text);
//        String code = getIntent().getStringExtra("code");
//        textView.setText(String.format("The true code is: %s", code));


        Button visitStart_button = (Button) findViewById(R.id.visitStart_button);
        Button visitPlan_button = (Button) findViewById(R.id.visitPlan_button);
        Button patientAdd_Button = (Button) findViewById(R.id.patientAdd_button);
        Button patientDataBase_Button = (Button) findViewById(R.id.patientDataBase_button);
        Button exitButton = (Button) findViewById(R.id.exit_button);


        exitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent logOut = new Intent(MainActivity.this, LockActivity.class);
                startActivity(logOut);
            }
        });

        visitStart_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(new Intent(MainActivity.this, StartVisitActivity.class)));
            }
        });


        visitPlan_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(new Intent(MainActivity.this, VisitPlanActivity.class)));
            }
        });

        patientAdd_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(new Intent(MainActivity.this, AddPatientActivity.class)));
            }
        });

        patientDataBase_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(new Intent(MainActivity.this, PatientBaseActivity.class)));
            }
        });

    }
}