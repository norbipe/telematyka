package com.norbipe;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;

public class StartVisitActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_startvisit);


        final Chronometer timer = (Chronometer) findViewById(R.id.timer);
        timer.start();


//        @Override
//        protected void onStop () {
//            super.onStop();
//        }
//
//        @Override
//        protected void onStart () {
//            super.onStart();
//            timer.start();
//        }


    }


}
